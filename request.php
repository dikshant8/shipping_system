<!DOCTYPE html><!-- Request Page for ShipOnline -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="WAD Assignment-1" />
		<meta name="keywords" content="Php and Mysql" />
		<meta name="author" content="Dikshant Bawa" />
		<title> ShipOnline System </title>
	</head>
	<body bgcolor="#FFFF99">
		<h1> ShipOnline System Request Page</h1>
		<!--Request Page for Customers-->		
		<form id="request" method="post" action="request.php?c_id=<?php echo $_GET['c_id']; ?>"><!-- setting parameters for customer id when user submits --> 
			<fieldset> 
				<p><strong><em>Item Information:</em></strong></p>
				<fieldset>
					<p>
						<label for="description">Item Description:</label>
						<input type="text" name="description" id="description" size="75"/>
					 </p> 
					<p>
						<label for="weight">Weight:</label>
						<select name="weight" id="weight">
							<option value="null" selected="selected">Select Weight</option>
							<option value="2">0-2 Kg</option>
							<?php for($i=3;$i<=20;$i++)// loop for select options of weight box
								{
									echo "<option value='",$i,"'>",$i," Kg</option>";
								}
							?>		
						</select>
					</p>
				</fieldset>
				<p><strong><em>Pick-up Information:</em></strong></p>
				<fieldset>
					<p>
						<label for="address">Address:</label>
						<input type="text" name="address" id="address" size="75"/>
					 </p> 
					 <p>
						<label for="suburb">Suburb:</label>
						<input type="text" name="suburb" id="suburb" size="30"/>
					 </p> 
					<p>
						<label for="date">Preferred Date:</label>
						<select name="day" id="day">
							<option value="null" selected="selected">Day</option>
							<?php for($i=1;$i<=31;$i++)//loop for day options
								{
									echo "<option value='",$i,"'>",$i,"</option>";
								}
							?>		
						</select>
						<select name="month" id="month">
							<option value="null" selected="selected">Month</option>
							<option value="1">Jan</option>
							<option value="2">Feb</option>
							<option value="3">Mar</option>
							<option value="4">Apr</option>
							<option value="5">May</option>
							<option value="6">Jun</option>
							<option value="7">Jul</option>
							<option value="8">Aug</option>
							<option value="9">Sept</option>
							<option value="10">Oct</option>
							<option value="11">Nov</option>
							<option value="12">Dec</option>	
						</select>
						<select name="year" id="year">
							<option value="null" selected="selected">Year</option>
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>	
						</select>
					</p>
					<p>
						<label for="time">Preferred Time:</label>
						<select name="hour" id="hour">
							<option value="null" selected="selected">Hour</option>
							<?php for($i=7;$i<=20;$i++)// loop for hour option from 7 -20 
								{
									echo "<option value='",$i,"'>",$i,"</option>";
								}
							?>		
						</select>
						<label for="minute">Minute:</label>
						<input type="text" name="minute" id="minute"/>
						<p><small>If you don't input minute properly, we'll assume you want us to pick the item up at the exact hour.</small></p>
					</p>
				</fieldset>
				<p><strong><em>Delivery Information:</em></strong></p>
				<fieldset>
					<p>
						<label for="receivername">Receiver Name:</label>
						<input type="text" name="receivername" id="receivername" size="30"/>
					 </p> 
					 <p>
						<label for="receiveraddress">Address:</label>
						<input type="text" name="receiveraddress" id="receiveraddress" size="75"/>
					 </p> 
					 <p>
						<label for="receiversuburb">Suburb:</label>
						<input type="text" name="receiversuburb" id="receiversuburb" size="30"/>
					 </p> 
					 <p>
						<label for="state">State</label>
						<select name="state" id="state">
							<option value="null" selected="selected">Select State</option>
							<option value="VIC">VIC</option>
							<option value="NSW">NSW</option>
							<option value="QLD">QLD</option>
							<option value="NT">NT</option>
							<option value="WA">WA</option>
							<option value="SA">SA</option>
							<option value="TAS">TAS</option>
							<option value="ACT">ACT</option>
						</select>
					</p>
				</fieldset>
				<!--Sending the Customer_id hidden type in request form-->
				 <input type="hidden" name="id" value="<?php echo $_GET['c_id']; ?>">
				<p>
					<input type="submit" value="Request" />
				</p>
				<?php //Checking if all the fields in the request form are set
					if(isset($_POST['description']) && isset($_POST['weight']) && isset($_POST['address']) && isset($_POST['suburb'])
						&& isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year']) && isset($_POST['hour'])
						&& isset($_POST['minute']) && isset($_POST['receivername']) && isset($_POST['receiveraddress']) && isset($_POST['receiversuburb'])
						&& isset($_POST['state']))
					{
						require_once ("settings.php");// connecting with database
						$conn = @mysqli_connect($host,
							$user,
							$pwd,
							$sql_db
							);
						//checking the connection
						if(!$conn)
							echo "<p> Database connection failure</p>";
						else {	
							//getting data from request FORM
							$description = trim($_POST["description"]);
							$weight = trim($_POST["weight"]);
							$t=$weight;
							$cost = 0;
							//Calculating the Cost of the package
							if ($t == 2)
							{
								$cost = 10;
							}
							else if($t >2)
							{
								$t -=2;
								$cost = 10+($t*2);
							}	
							$address = trim($_POST["address"]);
							$suburb = trim($_POST["suburb"]);
							$day = trim($_POST["day"]);	
							$month = trim($_POST["month"]);
							$year = trim($_POST["year"]);
							$hour = trim($_POST["hour"]);
							$minute = trim($_POST["minute"]);
							//If user doesn't input minutes set minutes to 00.
							if ($minute == "")
								$minute ="00";
							$receivername = trim($_POST["receivername"]);	
							$receiveraddress = trim($_POST["receiveraddress"]);
							$receiversuburb = trim($_POST["receiversuburb"]);
							$state = trim($_POST["state"]);
							$customer_id= $_POST["id"];
							$time = $hour.":".$minute;						
							$starttime = "07:30";//picking startup time
							$stoptime = "20:30";//picking end time		
							if(is_numeric($minute) == true)
							{
								//checking if all fields are entered
								if(!(($description=="") || ($weight=='null') || ($address=="") || ($suburb=="") || ($day=='null') || ($month=='null')
								|| ($year=='null')|| ($hour=='null')|| ($receivername=="")|| ($receiveraddress=="")|| ($receiversuburb=="")|| ($state=='null')))
								{
									$d=mktime(0,0,0,$month,$day,$year);
									$pickup_date=date("Y-m-d",$d);//Making Date format from Select input option
									if($pickup_date > date("Y-m-d"))//Checking Date
									{	
										//Checking the pickup time
										if((date ('H:i',strtotime($time)) > date ('H:i',strtotime($starttime))) &&
																	(date ('H:i',strtotime($time)) < date ('H:i',strtotime($stoptime))))
										{
											//Checking if pickuptime is 24 hours after current time
											if(($pickup_date == date('Y-m-d', strtotime(' +1 day'))) && (date ('H:i',strtotime($time)) < date("H:i")))
											{
												echo "<p> Enter the preferred date and time 24 hours after the current date and time.</p>" ;
											}	
											else
											{
												$sql_table="request";
												// creating table request if it doesnt exist
												$sqlString = "show tables like '$sql_table'"; 
												$result = @mysqli_query($conn, $sqlString);
												if(mysqli_num_rows($result)==0) {
													$sqlString = "create table request (request_number int not null auto_increment primary key,request_date date not null,
														customer_id int not null,item_description varchar(50) not null,weight int not null, 
														pickup_address varchar(30) not null, pickup_suburb varchar(30) not null,pickup_date date not null,
														pickup_time varchar(20) not null, receiver_name varchar(30) not null,
														receiver_address varchar(30) not null,receiver_suburb varchar(30) not null, state varchar(10) not null,
														FOREIGN KEY (customer_id) REFERENCES Customer(customer_id))"; 
													$result2 = @mysqli_query($conn, $sqlString);
												} 		
												//mySQL command for inserting the values
												$query="insert into $sql_table (customer_id,request_date,item_description,weight,pickup_address,pickup_suburb,pickup_date,
												pickup_time,receiver_name,receiver_address, receiver_suburb,state)values ('$customer_id',CURDATE(),'$description',
												'$weight','$address','$suburb','$pickup_date','$time','$receivername','$receiveraddress','$receiversuburb','$state')";
												$result=mysqli_query($conn, $query);//inserting data into table
												//checks if connection was successful
												if(!$result)
													echo "<p> Something is wrong with ",$query,"</p>";
												else 
												{
													$query2="select request_number,pickup_date,pickup_time from request where customer_id='$customer_id'
													AND item_description = '$description' AND weight='$weight'";
													$result2=mysqli_query($conn, $query2);
													while($row = mysqli_fetch_assoc($result2))
													{
														$number = $row["request_number"];
														$pickup_date = $row["pickup_date"];
														$pickup_time = $row["pickup_time"];
														$query3="select email,customername  from customer where customer_id='$customer_id'";
														$result3=mysqli_query($conn, $query3);
														while($row2 = mysqli_fetch_assoc($result3))
														{
															//Mail function to send the confirmation mail to the user
															$to = $row2["email"];
															$name=$row2["customername"];
															$subject = "shipping request with ShipOnline";
															$message = "Dear $name, Thank you for using ShipOnline! Your request number is $number. The cost is  $$cost. We will pick-up the item at $pickup_time on $pickup_date.";
															$headers = "From: Shipping Online System <4942892@student.swin.edu.au>";
															mail($to, $subject, $message, $headers, "-r 4942892@student.swin.edu.au");
														}	
														//Displaying the message with request number
														echo "<p>Thank you! Your request number is <strong>$number</strong>. The cost is $<strong>$cost</strong>.
														We will pick-up the item at <strong>$pickup_time</strong> on <strong>$pickup_date</strong>.</p>";
														mysqli_free_result($result3);//Allocated Result memory is freed
													}	
													mysqli_free_result($result2);//Allocated Result memory is freed		
												}
											}
										}
										else
											echo "<p>Please enter a Pickup time between 07:30 - 20:30pm </p>";
									}
									else
										echo "<p> Enter the preferred date and time 24 hours after the current date and time.</p>" ;
								}		
								else
									echo"<p> Please Enter all the fields in the form. </p>";
							}
							else
								echo"<p> Please Enter proper Minutes in the form. </p>";
							mysqli_close($conn);//database connection closed
						}
					}	
				?>
			</fieldset>
		</form>
		<a href ="shiponline.php">Home</a>
	</body>
</html>