<!DOCTYPE html><!-- Customer Login Page for Ship Online System -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="WAD Assignment-1" />
		<meta name="keywords" content="Php and Mysql" />
		<meta name="author" content="Dikshant Bawa" />
		<title> ShipOnline System </title>
	</head>
	<body bgcolor="#FFFF99">
		<h1> ShipOnline System Login Page</h1>
		<!--Login Page form to use Ship Online System-->
		<form id="login" method="post" action=" login.php" >
			<fieldset> 
				<p>
					<label for="customernumber">Customer Number: </label>
					<input type="text" name="customernumber" id="customernumber"/>
				</p> 
				<p><label for="pwd">Password: </label>
					<input type="password" name="pwd" id="pwd"/>
				</p> 
				<p>
					<input type="submit" value="Register" />
				</p>
				<?php
					
					if(isset($_POST['customernumber']) && isset($_POST['pwd']))//checking if fields are set 
					{
						require_once ("settings.php");//connecting with database
						$conn = @mysqli_connect($host,
							$user,
							$pwd,
							$sql_db
							);
						//checking the connection
						if(!$conn){
							echo "<p> Database connection failure</p>";
						}
						else {	
							//getting data from login FORM
							$customernumber = trim($_POST["customernumber"]);
							$pwd = trim($_POST["pwd"]);		
							//Getting Cutomer_numbers from table
							$query="select customer_id from customer";
							if(!(($customernumber=="") || ($pwd==""))) {// checking if fields are entered
								$result=mysqli_query($conn, $query);
								if(!$result)
									echo "<p> Something is wrong with ",$query,"</p>";
								else
								{								
									while($row = mysqli_fetch_assoc($result))
									{
										if($customernumber == $row["customer_id"]) //checking if customernumber exists in table
										{
											//getting password for entered customer number from table
											$query2 = "select password from customer where customer_id='$customernumber'";
											$result2=mysqli_query($conn, $query2);
											if(!$result2)
												echo "<p>Customer number and Password do not match</p>";
											while($row2 = mysqli_fetch_assoc($result2))
											{
												if(strcmp($pwd,$row2["password"]) == 0) //checking the password
												{
													//going to request page with customer_id as parameters
													header('Location: https://mercury.ict.swin.edu.au/cos80021/s4942892/Assignment1/request.php?c_id='.$customernumber);
												}
											}
											mysqli_free_result($result2);	//Allocated Result memory is freed
										}
									}
								}	
								echo "<p> Customer number and Password do not match</p>";								
							}	
							else
								echo"<p> Enter the Customer Number and Password</p>";
							mysqli_close($conn);//closing the database connection
						}
					}	
				?>
			</fieldset>
		</form>
		<a href ="shiponline.php">Home</a>
	</body>
</html>