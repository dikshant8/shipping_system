<!DOCTYPE html><!-- Admin Page for Ship Online System -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="WAD Assignment-1" />
		<meta name="keywords" content="Php and Mysql" />
		<meta name="author" content="Dikshant Bawa" />
		<title> ShipOnline System </title>
	</head>
	<body bgcolor="#FFFF99">
		<h1> ShipOnline System Administration Page</h1>
		<!--Admin Page form for Ship Online System-->
		<form id="admin" method="post" action=" admin.php" >
			<fieldset> 
			<!-- Date input-->
				<p>
					<label for="date">Date for Retrieve:</label>
					<select name="day" id="day">
						<option value="null" selected="selected">Day</option>
						<?php for($i=1;$i<=31;$i++)//loop for day options
						{
							echo "<option value='",$i,"'>",$i,"</option>";
						}
						?>		
					</select>
					<select name="month" id="month">
						<option value="null" selected="selected">Month</option>
						<option value="1">Jan</option>
						<option value="2">Feb</option>
						<option value="3">Mar</option>
						<option value="4">Apr</option>
						<option value="5">May</option>
						<option value="6">Jun</option>
						<option value="7">Jul</option>
						<option value="8">Aug</option>
						<option value="9">Sept</option>
						<option value="10">Oct</option>
						<option value="11">Nov</option>
						<option value="12">Dec</option>	
					</select>
					<select name="year" id="year">
						<option value="null" selected="selected">Year</option>
						<option value="2014">2014</option>
						<option value="2015">2015</option>
						<option value="2016">2016</option>
						<option value="2017">2017</option>	
					</select>
				</p>
				<!-- Radio button to select Date-->
				<p> Select Date Item for Retrieve: 
					<input type="radio" name="retreivedate" id="request" class="dateoption" value="request" checked="checked"/>
					<label for="requestdate">Request Date</label>
					<input type="radio" name="retreivedate" id="pickup" class="dateoption" value="pickup" />
					<label for="pickupdate">Pick-up Date</label>
				</p>
				<p>
					<input type="submit" value="Show" />
				</p>
				<?php
					if(isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year']))// checking if all fields are set
					{
						$day = trim($_POST["day"]);	
						$month = trim($_POST["month"]);
						$year = trim($_POST["year"]);
						$dateoption = trim($_POST["retreivedate"]);
						require_once ("settings.php");
						$conn = @mysqli_connect($host,
								$user,
								$pwd,
								$sql_db
						);			
						//checking the connection
						if(!$conn)
							echo "<p> Database connection failure</p>";
						else if(!(($day=='null') || ($month=='null') || ($year=='null'))) //checking if user has select a date
						{		
							$n=0;
							$revenue=0;
							if($dateoption == "request")//checking the user option of requestdate or pickupdate
							{
								$d=mktime(0,0,0,$month,$day,$year);
								$requestdate=date("Y-m-d",$d);					
								$query="select * from request where request_date='$requestdate'"; //query to retreive data from request table
								$result = mysqli_query($conn, $query);
								if(!($result)) {
									echo "<p> Something is wrong with",$query,"</p>";
								} 
								else if(mysqli_num_rows($result)==0)
									echo "<p>No results Found on this particular <strong>$requestdate</strong></p>";
								else
								{
									//Displaying the records
									echo"<table id='request' border=\"1\">";
									echo "<tr>"
									."<th scope=\"col\">Customer Number</th>"
									."<th scope=\"col\">Request Number</th>"
									."<th scope=\"col\">Item Description</th>"
									."<th scope=\"col\">Weight</th>"
									."<th scope=\"col\">Pick-up Suburb</th>"
									."<th scope=\"col\">Pick-up Date</th>"
									."<th scope=\"col\">Delivery Suburb</th>"
									."<th scope=\"col\">State</th>"
									."</tr>";
									//retrieving Record from pointer
									while($row = mysqli_fetch_assoc($result)){
										echo "<tr>";
										echo "<td>",$row["customer_id"],"</td>";
										echo "<td>",$row["request_number"],"</td>";
										echo "<td>",$row["item_description"],"</td>";
										echo "<td>",$row["weight"],"</td>";
										echo "<td>",$row["pickup_suburb"],"</td>";
										echo "<td>",$row["pickup_date"],"</td>";
										echo "<td>",$row["receiver_suburb"],"</td>";
										echo "<td>",$row["state"],"</td>";
										echo "</tr>";
										$n++;
										$cost=0;
										//Calculation Total Revenue of all requests
										if($row["weight"] == 2)
											$cost=10;
										else if($row["weight"]>2)
										{
											$row["weight"] -=2;
											$cost = 10+($row["weight"]*2);
										}	
										$revenue+=$cost;	
									}
									echo "</table>";
									echo "<p> Total Number of Requests on given date <strong>$requestdate</strong> is <strong>$n</strong>.</p>";
									echo "<p> Total Revenue collected from all the Requests on given date <strong>$requestdate</strong> is $<strong>$revenue</strong>.</p>";
									mysqli_free_result($result);//Allocated Result memory is freed
								}			
								mysqli_close($conn);
							}	
							else
							{
								$d=mktime(0,0,0,$month,$day,$year);
								$pickupdate=date("Y-m-d",$d);	
								$n=0;	
								$w=0;					
								// Requesting data from two tables using Left join on customer table								
								$query="select * from request left join customer on (request.customer_id=customer.customer_id) 
								where pickup_date='$pickupdate' order by pickup_suburb,state,receiver_suburb"; 
								$result = mysqli_query($conn, $query);
								if(!($result)) {
									echo "<p> Something is wrong with",$query,"</p>";
								} 
								else if(mysqli_num_rows($result)==0)
									echo "<p>No results Found on this particular <strong>$pickupdate</strong></p>";
								else{
									//Displaying the records
									echo"<table id='request' border=\"1\">";
									echo "<tr>"
									."<th scope=\"col\">Customer Number</th>"
									."<th scope=\"col\">Customer Name</th>"
									."<th scope=\"col\">Customer Contact</th>"
									."<th scope=\"col\">Request Number</th>"
									."<th scope=\"col\">Item Description</th>"
									."<th scope=\"col\">Weight</th>"
									."<th scope=\"col\">Pick-up Address</th>"
									."<th scope=\"col\">Pick-up Suburb</th>"
									."<th scope=\"col\">Pick-up Time</th>"
									."<th scope=\"col\">Delivery Suburb</th>"
									."<th scope=\"col\">State</th>"
									."</tr>";
									//retrieving Record from pointer
									while($row = mysqli_fetch_assoc($result)){
										echo "<tr>";
										echo "<td>",$row["customer_id"],"</td>";
										echo "<td>",$row["customername"],"</td>";
										echo "<td>",$row["phone"],"</td>";
										echo "<td>",$row["request_number"],"</td>";
										echo "<td>",$row["item_description"],"</td>";
										echo "<td>",$row["weight"],"</td>";
										echo "<td>",$row["pickup_address"],"</td>";
										echo "<td>",$row["pickup_suburb"],"</td>";
										echo "<td>",$row["pickup_time"],"</td>";
										echo "<td>",$row["receiver_suburb"],"</td>";
										echo "<td>",$row["state"],"</td>";
										echo "</tr>";
										$n++;
										$w+=$row["weight"];//Total weight on that pickupdate
									}
									echo "</table>";
									echo "<p> Total Number of Requests on given pickup date <strong>$pickupdate</strong> is <strong>$n</strong>.</p>";
									echo "<p> Total Weight of all the Requests on given pickup date <strong>$pickupdate</strong> is <strong>$w</strong>.</p>";
									mysqli_free_result($result);//Allocated Result memory is freed
								}			
								mysqli_close($conn);
							}
						}	
						else
							echo "<p> Enter the date for which you want to see the requests </p>" ;
					}		
				?> 	
			</fieldset>
		</form>
		<a href ="shiponline.php">Home</a>
	</body>
</html>