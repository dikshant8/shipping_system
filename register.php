<!DOCTYPE html><!-- Customer Registration Form -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="WAD Assignment-1" />
		<meta name="keywords" content="Php and Mysql" />
		<meta name="author" content="Dikshant Bawa" />
		<title> ShipOnline System </title>
	</head>
	<body bgcolor="#FFFF99">
		<h1> ShipOnline System Registration Page</h1>
		<!--Registration Form for Customers-->
		<form id="registration" method="post" action=" register.php" >
			<fieldset> 
				<p>
					<label for="customername">Name: </label>
					<input type="text" name="customername" id="customername"/>
				</p> 
				<p><label for="pwd">Password: </label>
					<input type="password" name="pwd" id="pwd"/>
				</p> 
				<p><label for="confirmpwd">Confirm Password: </label>
					<input type="password" id="confirmpwd" name="confirmpwd" />
				</p>	
				<p><label for="email">Email Address: </label>
					<input type="email" placeholder= "your_email@example.org.me" id="email" name="email" size="40" />
				</p>
				<p>
					<label for="phone">Contact Phone: </label>
					<input type="text" id="phone"  name="phone" />
				</p>
				<p>
					<input type="submit" value="Register" />
				</p>
				<?php //Checking if all fields are set
					if(isset($_POST['customername']) && isset($_POST['pwd']) && isset($_POST['confirmpwd']) && isset($_POST['email']) && isset($_POST['phone']))
					{
						require_once ("settings.php");// connecting with database
						$conn = @mysqli_connect($host,
							$user,
							$pwd,
							$sql_db
							);
						//checking the connection
						if(!$conn){
							echo "<p> Database connection failure</p>";
						}
						else {	
							//getting data from registration FORM
							$customername = trim($_POST["customername"]);
							$pwd = trim($_POST["pwd"]);
							$confirmpwd = trim($_POST["confirmpwd"]);
							$email = trim($_POST["email"]);
							$phone = trim($_POST["phone"]);		
							$sql_table="customer";
							// creating table Customer if it doesnt exist
							$sqlString = "show tables like '$sql_table'"; 
							$result = @mysqli_query($conn, $sqlString);
							if(mysqli_num_rows($result)==0) {
								$sqlString = "create table customer (customer_id int not null auto_increment primary key,customername varchar(30) not null,
								password varchar(20) not null,email varchar(30) not null, phone int(20) not null)"; 
								$result2 = @mysqli_query($conn, $sqlString);
							} 		
							//mySQL command for inserting the values
							$query="insert into $sql_table (customername,password,email,phone)
							values ('$customername','$pwd','$email','$phone')";
							if(!(($customername=="") || ($pwd=="") || ($confirmpwd=="") || ($email=="") || ($phone==""))) {// checking all fields are entered
								if($pwd == $confirmpwd)//checking if password match
								{
									if(is_numeric($phone) == true) //checking if phone number is valid digits
									{
										$query2="select email from customer where email='$email'";
										$result2=mysqli_query($conn, $query2);
										if(mysqli_num_rows($result2)==0)//Checking if email address exist in the database
										{
											$result=mysqli_query($conn, $query);//inserting data into table
											//checks if connection was successful
											if(!$result)
												echo "<p> Something is wrong with ",$query,"</p>";
											else 
											{
												$query3="select customer_id from customer where email='$email'";
												$result3=mysqli_query($conn, $query3);
												while($row = mysqli_fetch_assoc($result3))
												{
													//Displaying the registration complete with customer id
													$id = $row["customer_id"];
													echo "<p>Dear <strong>$customername</strong>, you are successfully registered into ShipOnline, and your customer
													number is <strong>$id</strong> , which will be used to get into the system.</p>";
												}	
												mysqli_free_result($result3);	//Allocated Result memory is freed
											}
										}	
										else
											echo"<p> This Email Address already exist in our database.</p>";
									}
									else
										echo"<p> Enter a valid phone number</p>";
								}
								else 
								{
									echo "<p>Passwords do not match</p>";
								}
							}	
							else
								echo"<p> Enter all the fields in the form</p>";
							mysqli_close($conn);//database connection closed
						}
					}	
				?>
			</fieldset>
		</form>
		<a href ="shiponline.php">Home</a>
	</body>
</html>