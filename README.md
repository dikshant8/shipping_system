# README #

# Ship Online System #
ShipOnline System is developed using PHP and MySQl technology. It is an online shipping system where users can make request to pickup an item from certain address and then deliver the item to another address. The system charges users standard price based on weight of the package.

* shiponline.php
This is the homepage of Ship Online system. New customers can register by clicking the Registration link.
 Existing users can Log-In the system and can make request to ship their item.
Administrator can click Administration link and check the requests made for a particular date and total revenue on that particular date.
 
* register.php
This page in the system is used to register new customers in the system. For each customer an automatically customer_id is generated which serves the primary key in the table. The email Id of customers are unique. Same Email Id are not accepted in the database.

* login.php
This webpage is created to authenticate if the user has registered in the system before they created any request. Registered customers have to enter their customer number and password to enter this page. If it matches the database in the table it will redirect the customer to request.php.

* request.php
After login, customers will be redirected to this page. Here they can fill in the request form to ship their items. Customers have to specify weight on the basis of what Cost of delivery is calculated. Customers can only enter a Pickup time after 24 hours from the current date and time. The pickup time for item is between 07:30 am- 20:30 pm. After user submits, it will send the confirmation email to user on their email id. In case the email id is not correct the email will bounce back to the admin email id.

* admin.php
On this page administrator can see the requests made for a particular date. Administrator can retrieve request based on particular request date or pick up date
It also shows the total number of requests on that particular date and total revenue for that date.